﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    interface IRepository<T> where T: Entity
    {
        void Add(T entity);

        void Remove(T entity);

        T GetId(Guid id);

    }
}
