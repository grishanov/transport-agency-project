﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    public class PointsOfDestinationRepository:IRepository<PointOfDestination>
    {
        private List<PointOfDestination> pointsOfDestination;

        public PointsOfDestinationRepository()
        {
            pointsOfDestination = new List<PointOfDestination>();
        }

        public void Add(PointOfDestination entity)
        {
            pointsOfDestination.Add(entity);
        }

        public void Remove(PointOfDestination entity)
        {
            pointsOfDestination.Remove(entity);
        }

        public PointOfDestination GetId(Guid id)
        {
            var result = pointsOfDestination.AsQueryable().First(pOfD => pOfD.Id == id);
            return result;
        }

        public IEnumerable<PointOfDestination> GetAll()
        {
            return pointsOfDestination;
        }

      
    }
}
