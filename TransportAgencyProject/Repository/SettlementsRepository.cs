﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    public class SettlementsRepository:IRepository<Settlement>
    {
        private List<Settlement> settlements;

        public SettlementsRepository()
        {
            settlements = new List<Settlement>();
        }

        public void Add(Settlement entity)
        {
            settlements.Add(entity);
        }

        public void Remove(Settlement entity)
        {
            settlements.Add(entity);
        }

        public Settlement GetByTownName(string townName)
        {
            foreach(var settlement in settlements)
            {
                if(settlement.Town.Name.Equals(townName))
                {
                    return settlement;
                }
            }
            return null;
        }

        public Settlement GetId(Guid id)
        {
            var result = settlements.AsQueryable().First(cl => cl.Id == id);
            return result;
        }
    }
}
