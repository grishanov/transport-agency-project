﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    public class VehiclesRepository:IRepository<Vehicle>
    {
        private List<Vehicle> vehicles;

        public VehiclesRepository()
        {
            vehicles = new List<Vehicle>();
        }

        public void Add(Vehicle entity)
        {
            vehicles.Add(entity);
        }

        public void Remove(Vehicle entity)
        {
            vehicles.Add(entity);
        }

        public List<Vehicle> GetAll 
        {
            get { return vehicles; }
        }

        public Vehicle GetId(Guid id)
        {
            var result = vehicles.AsQueryable().First(cl => cl.Id == id);
            return result;
        }
    }
}
