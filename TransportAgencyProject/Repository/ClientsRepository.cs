﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    public class ClientsRepository:IRepository<Client>
    {
        private List<Client> clients; 
        
        public ClientsRepository()
        {
            clients = new List<Client>();
        }

        public void Add(Client entity)
        {
            clients.Add(entity);
        }

        public void Remove(Client entity)
        {
            clients.Add(entity);
        }

        public Client GetId(Guid id)
        {
            var result = clients.AsQueryable().First(cl => cl.Id == id);
            return result;
        }

        public IEnumerable<Client> GetAllClients()
        {
            return clients;
        }
    }
}
