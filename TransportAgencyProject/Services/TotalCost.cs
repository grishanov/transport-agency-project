﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace Services
{
    public static class TotalCost
    {
        static public double CalculateCost(PointOfDestination pointOfDestination)
        {
            return pointOfDestination.Hotel.DailyCost*pointOfDestination.ResidenceTime
                   + pointOfDestination.Vehicle.Cost + pointOfDestination.Service.Car.Cost +
                   pointOfDestination.Service.Insurance.Cost;
        }
    }
}
