﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class SetPointOfDestination:IAction
    {
        private Town town;
        private Vehicle vehicle;
        private TravelyPackageClass travelyPackageClass;

        public SetPointOfDestination(Town town, Vehicle vehicle, TravelyPackageClass travelyPackageClass)
        {
            this.town = town;
            this.vehicle = vehicle;
            this.travelyPackageClass = travelyPackageClass;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Турплан выбран.");

            context.Out.Write("Сколько дней вы предполагаете проживать: ");
            var allDays = context.In.ReadLine();
            Int32 residenceTime = 0;
            Int32.TryParse(allDays, out residenceTime);

            List<TravelyPackage> listOfTPs = new List<TravelyPackage>();
            var travelyPackagedConfigurator = new TravelyPackageConfigurator();
            if ((int) travelyPackageClass == -1)
            {
                context.Out.Write("Введите минимальную цену: ");
                var minPrice = context.In.ReadLine();
                double minPriceDouble = 0;
                Double.TryParse(minPrice, out minPriceDouble);

                context.Out.Write("Введите максимальную цену: ");
                var maxPrice = context.In.ReadLine();
                double maxPriceDouble = 0;
                Double.TryParse(maxPrice, out maxPriceDouble);

                listOfTPs =
                    travelyPackagedConfigurator.GetByCost(Program.settlementsRepository.GetByTownName(town.Name),
                        minPriceDouble, maxPriceDouble, residenceTime);
            }
            else
            {
                listOfTPs =
                    travelyPackagedConfigurator.GetByClass(Program.settlementsRepository.GetByTownName(town.Name),
                        travelyPackageClass);
            }

            var submenu = new MenuBuilder()
              .Title("Доступные турпакеты.")
              .Prompt("Укажите пакет:")
              .RunnableOnce();
            foreach (var packet in listOfTPs)
            {
                submenu.Item("hotel: " + packet.Hotel.Name + " insurance: " + packet.Insurance.Number + " car: " + packet.PrivateCar.Model,
                    new TheGoal(residenceTime, vehicle, Program.settlementsRepository.GetByTownName(town.Name),packet ));
            }
            submenu
                .Exit("Отмена")
                .GetMenu().Run();

        }
    }
}
