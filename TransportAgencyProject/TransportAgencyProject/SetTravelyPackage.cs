﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class SetTravelyPackage:IAction
    {
        Town town;
        private Vehicle vehicle;

        public SetTravelyPackage(Town town, Vehicle vehicle)
        {
            this.town = town;
            this.vehicle = vehicle;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Вы выбрали " + vehicle.Name + " чтобы добраться до " + town.Name + "." );
            var menu = new MenuBuilder()
                .Title("Составление турплана.")
                .Prompt("По какому принципу составить план:")
                .RunnableOnce();
            foreach (var travelyClass in Enum.GetNames(typeof (TravelyPackageClass)))
            {
                menu.Item(travelyClass,
                    new SetPointOfDestination(town, vehicle,
                        (TravelyPackageClass) Enum.Parse(typeof (TravelyPackageClass), travelyClass)));
            }
            menu.Item("Ввести диапазон цен вручную", new SetPointOfDestination(town, vehicle, (TravelyPackageClass) (-1)));
            menu
                .Exit("Отмена")
                .GetMenu().Run();
            
        }
    }
}
