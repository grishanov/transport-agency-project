﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Menu.Builders;
using Repository;

namespace TransportAgencyProject
{
    class Program
    {
        public static VehiclesRepository vehiclesRepository = new VehiclesRepository();
        public static ClientsRepository clientsRepository = new ClientsRepository();
        public static SettlementsRepository settlementsRepository = new SettlementsRepository();
        public static PointsOfDestinationRepository pointsOfDestinationRepository = new PointsOfDestinationRepository();
        public static List<Town> towns = new List<Town>(); 

        static void FillRepositories()
        {
            Town Moscow = new Town("Москва");
            Town NewYork = new Town("Нью-Йорк");
            Town Dubai = new Town("Дубаи");
            Town Pekin = new Town("Пекин");
            Town Minsk = new Town("Минск");
            towns.Add(Moscow);
            towns.Add(NewYork);
            towns.Add(Dubai);
            towns.Add(Pekin);
            towns.Add(Minsk);
            

            vehiclesRepository.Add(new Vehicle("А-29", VehicleType.Airplane, VehicleClass.VeryBad, 3000,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("И-211", VehicleType.Airplane, VehicleClass.Bad, 3500,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Ар-12", VehicleType.Airplane, VehicleClass.Middle, 4000,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("СУ-150", VehicleType.Airplane, VehicleClass.Good, 4500,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Ав-45", VehicleType.Airplane, VehicleClass.VeryBad, 5000,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Ик-5", VehicleType.Airplane, VehicleClass.Middle, 2500,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Ду-500", VehicleType.Airplane, VehicleClass.Good, 3200,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Тк-10", VehicleType.Airplane, VehicleClass.VeryGood, 6000,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));
            vehiclesRepository.Add(new Vehicle("Ри-18", VehicleType.Airplane, VehicleClass.Bad, 1600,
                new List<Town> { Moscow, NewYork, Dubai, Pekin, Minsk }));

            vehiclesRepository.Add(new Vehicle("Экспресс Москва-Минск", VehicleType.Train, VehicleClass.VeryGood, 3000,
                new List<Town> { Moscow,Minsk}));
            vehiclesRepository.Add(new Vehicle("Поезд Москва-Минск", VehicleType.Train, VehicleClass.Middle, 2000,
                new List<Town> { Moscow, Minsk }));

            vehiclesRepository.Add(new Vehicle("Автобус Москва-Минск", VehicleType.Bus, VehicleClass.Bad, 700,
                new List<Town> { Moscow, Minsk }));
            vehiclesRepository.Add(new Vehicle("Автобус Минск-Москва", VehicleType.Bus, VehicleClass.Bad, 300,
                new List<Town> { Moscow, Minsk }));

            vehiclesRepository.Add(new Vehicle("Пароход",VehicleType.Ship,VehicleClass.VeryGood,5600,
                new List<Town> { Pekin,Dubai}));
            vehiclesRepository.Add(new Vehicle("Яхта", VehicleType.Ship, VehicleClass.VeryGood, 6500,
                new List<Town> { Pekin, Dubai, NewYork }));
            vehiclesRepository.Add(new Vehicle("Лодка", VehicleType.Ship, VehicleClass.VeryGood, 6500,
                new List<Town> { Pekin, Moscow, Minsk }));

            List<Hotel> MoscowHotels = new List<Hotel> { new Hotel("Москва", HotelClass.VeryGood, 1000),
                                                         new Hotel("3 звезды", HotelClass.Good, 800),
                                                         new Hotel("Столица", HotelClass.Middle, 500),
                                                         new Hotel("Столичный отель", HotelClass.Good, 650),
                                                         new Hotel("Отель 5 звезд", HotelClass.VeryGood, 1500),
                                                         new Hotel("Гостиница",HotelClass.VeryBad,450)   };

            List<Hotel> NewYorkHotels = new List<Hotel> { new Hotel("New-York", HotelClass.VeryGood, 1000),
                                                         new Hotel("3 stars", HotelClass.Good, 800),
                                                         new Hotel("NativeLand", HotelClass.Middle, 500),
                                                         new Hotel("Motel", HotelClass.Good, 650),
                                                         new Hotel("Otel 5 stars", HotelClass.VeryGood, 1500),
                                                         new Hotel("Hotel",HotelClass.VeryBad,450)   };

            List<Hotel> DubaiHotels = new List<Hotel> {  new Hotel("Dubai5stars", HotelClass.VeryGood, 1000),
                                                         new Hotel("3 stars", HotelClass.Good, 800),
                                                         new Hotel("NativeLand", HotelClass.Middle, 500),
                                                         new Hotel("Motel", HotelClass.Good, 650),
                                                         new Hotel("Otel 5 stars", HotelClass.VeryGood, 1500),
                                                         new Hotel("Hotel",HotelClass.VeryBad,450)   };

            List<Hotel> PekinHotels = new List<Hotel> {  new Hotel("ChinaPower", HotelClass.VeryGood, 1000),
                                                         new Hotel("3 stars", HotelClass.Good, 800),
                                                         new Hotel("NativeLand", HotelClass.Middle, 500),
                                                         new Hotel("Motel", HotelClass.Good, 650),
                                                         new Hotel("Otel 5 stars", HotelClass.VeryGood, 1500),
                                                         new Hotel("Hotel",HotelClass.VeryBad,450)   };

            List<Hotel> MinskHotels = new List<Hotel> { new Hotel("Минск", HotelClass.VeryGood, 1000),
                                                         new Hotel("3 звезды", HotelClass.Good, 800),
                                                         new Hotel("Столица", HotelClass.Middle, 500),
                                                         new Hotel("Столичный отель", HotelClass.Good, 650),
                                                         new Hotel("Отель 5 звезд", HotelClass.VeryGood, 1500),
                                                         new Hotel("Гостиница",HotelClass.VeryBad,450)   };

            List<PrivateCar> privateCars = new List<PrivateCar> { new PrivateCar("Lada Kalina",CarClass.D,500),
                                                                new PrivateCar("Lada Priora",CarClass.C,1000),
                                                                new PrivateCar("Lada Cranta",CarClass.E,800),
                                                                new PrivateCar("Lada 2112",CarClass.F,300),
                                                                new PrivateCar("Mazda 3",CarClass.C,950),
                                                                new PrivateCar("Mazda 6",CarClass.B,1200),
                                                                new PrivateCar("Mazda CX-9",CarClass.A,1500),
                                                                new PrivateCar("Toyota Corolla",CarClass.A,2000),
                                                                new PrivateCar("Moskvich",CarClass.M,120),
                                                                new PrivateCar("Audi 80",CarClass.D,750),
                                                                new PrivateCar("Audio 120",CarClass.C,13000)
            };

            List<Insurance> insurancis = new List<Insurance> { new Insurance(30,"12AF11",100),
                                                               new Insurance(45,"11FF91",200),
                                                               new Insurance(10,"2CU01",50),
                                                               new Insurance(15,"DCU01",50),
                                                               new Insurance(60,"51DY11",250),
                                                               new Insurance(75,"19HA89",300),
                                                               new Insurance(80,"29TR12",350)
            };

            settlementsRepository.Add(new Settlement(Minsk, MinskHotels, privateCars, insurancis));
            settlementsRepository.Add(new Settlement(Moscow, MoscowHotels, privateCars, insurancis));
            settlementsRepository.Add(new Settlement(Pekin, PekinHotels, privateCars, insurancis));
            settlementsRepository.Add(new Settlement(Dubai, DubaiHotels, privateCars, insurancis));
            settlementsRepository.Add(new Settlement(NewYork, NewYorkHotels, privateCars, insurancis));

            clientsRepository.Add(new Client("Софриков Андрей Иванович"));
            clientsRepository.Add(new Client("Иванов Сергей Петрович"));
            clientsRepository.Add(new Client("Кушнир Олег Сергеевич"));
        }

        static void Main(string[] args)
        {
            FillRepositories();
            new MenuBuilder()
                .Title("----------------------------------------------\n" +
                       "|#######                       #             |\n" +
                       "|   #                         # #            |\n" +
                       "|   # R A N S P O R T        # # # G E N C Y |\n" +
                       "|   #                       #     #          |\n" +
                       "----------------------------------------------\n")
                .Repeatable()
                .Item("Залогиниться", new LoginClient())
                .Item("Зарегистрироваться", new RegisterClient())
                .Exit("Выйти")
                .GetMenu().Run();
        }
    }
}
