﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class MadeVehicleClass:IAction
    {
        private Town town;
        private VehicleType vehicleType;

        public MadeVehicleClass(Town town, VehicleType vehicleType)
        {
            this.town = town;
            this.vehicleType = vehicleType;
        }

        public MadeVehicleClass(Town town)
        {
            this.town = town;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Вы выбрали для поездки " + vehicleType +".");
            var menu = new MenuBuilder()
              .Title("Каким классом можно добраться на " + vehicleType + " до " + town.Name + ".")
              .Prompt("Выберите класс:")
              .RunnableOnce();
            foreach (var vehicle in Enum.GetNames(typeof(VehicleClass)))
            {
                menu.Item(vehicle, new MadeVehicleByCost(town, vehicleType, (VehicleClass)Enum.Parse(typeof(VehicleClass), vehicle)));
            }
            menu
                .Exit("Отмена")
                .GetMenu().Run();
        }
    }
}
