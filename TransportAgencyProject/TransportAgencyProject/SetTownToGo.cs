﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class SetTownToGo:IAction
    {
        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Вы успешно вошли в систему.");
            var submenu = new MenuBuilder()
                .Title("Куда вы хотите поехать?")
                .Prompt("Выберите город:")
                .RunnableOnce();
            foreach (var town in Program.towns)
            {
                submenu.Item(town.Name, new MadeVehicleType(town));
            }
            submenu
               .Exit("Отмена")
               .GetMenu().Run();


        }
    }
}
