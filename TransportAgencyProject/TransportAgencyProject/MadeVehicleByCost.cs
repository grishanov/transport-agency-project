﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class MadeVehicleByCost:IAction
    {
        private Town town;
        private VehicleType vehicleType;
        private VehicleClass vehicleClass;

        public MadeVehicleByCost(Town town, VehicleType vehicleType, VehicleClass vehicleClass)
        {
            this.town = town;
            this.vehicleType = vehicleType;
            this.vehicleClass = vehicleClass;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Вы выбрали для поездки " + vehicleType +" " + "класса " + vehicleClass + ".");
            context.Out.WriteLine(ConsoleColor.Yellow, "Введите диапазон цен.");

            context.Out.Write("Введите минимальную цену: ");
            var minPrice = context.In.ReadLine();
            double minPriceDouble = 0;
            Double.TryParse(minPrice, out minPriceDouble);

            context.Out.Write("Введите максимальную цену: ");
            var maxPrice = context.In.ReadLine();
            double maxPriceDouble = 0;
            Double.TryParse(maxPrice, out maxPriceDouble);

            var configurator = new VehicleConfigurator();

            var goodVehiclesByType = configurator.GetByType(Program.vehiclesRepository.GetAll, vehicleType, town);
            var goodVehiclesByClass = configurator.GetByClass(goodVehiclesByType, vehicleClass, town);
            var goodVehiclesByAll = configurator.GetByCost(goodVehiclesByClass, minPriceDouble, maxPriceDouble, town);

            context.Out.WriteLine(ConsoleColor.Green, "Список доступного транспорта сформирован.");
            var submenu = new MenuBuilder()
            .Title("Список транспорта по вашим запросам.")
            .Prompt("Выберите из предложенного списка:")
            .RunnableOnce();
            foreach (var vehicle in goodVehiclesByAll)
            {
                submenu.Item(vehicle.Name, new SetTravelyPackage(town, vehicle));
            }
            submenu
                .Exit("Отмена")
                .GetMenu().Run();

            

        }
    }
}
