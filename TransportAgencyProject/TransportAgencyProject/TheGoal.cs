﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;

namespace TransportAgencyProject
{
    class TheGoal:IAction
    {
        private int residencetime;
        private Vehicle vehicle;
        private Settlement settlement;
        private TravelyPackage package;

        public TheGoal(int residencetime, Vehicle vehicle, Settlement settlement, TravelyPackage package)
        {
            this.residencetime = residencetime;
            this.vehicle = vehicle;
            this.settlement = settlement;
            this.package = package;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Ваш план поездки составлен. Приятного отдыха!");
            var randNumber = new Random();
            Program.pointsOfDestinationRepository.Add(new PointOfDestination(randNumber.Next().ToString(CultureInfo.InvariantCulture), DateTime.Now, residencetime, vehicle, settlement, settlement.Town, package ));
            foreach (var point in Program.pointsOfDestinationRepository.GetAll())
            {
                context.Out.WriteLine(ConsoleColor.Green, "Номер: " + point.Number + "Дата: " + point.Date
                                                           + "\nТранспорт:  " + point.Vehicle.Type + " " + point.Vehicle.Name + " " + point.Vehicle.VehicleClass 
                                                           + "\nПункт назначения: " + point.Settlement.Town.Name
                                                           + "\nВремя пребывания: " + point.ResidenceTime + " дней"
                                                           + "\nОтель: " + point.TravelyPackage.Hotel.Name
                                                           + "\nЛичное авто: "+ point.TravelyPackage.PrivateCar.Model
                                                           + "\nСтраховка: " + point.TravelyPackage.Insurance.Number + " " + point.TravelyPackage.Insurance.Percent + "%");
            }
            

        }
    }
}
