﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;
using Repository;


namespace TransportAgencyProject
{
    class LoginClient:IAction
    {
        public void Perform(ActionExecutionContext context)
        {
            var submenu = new MenuBuilder()
                .Title("Доступные клиенты")
                .Prompt("Укажите клиента:")
                .RunnableOnce();
            foreach (var client in Program.clientsRepository.GetAllClients())
            {
                submenu.Item(client.Name, new SetTownToGo());
            }
            submenu
                .Exit("Отмена")
                .GetMenu().Run();
            
        }
    }
}
