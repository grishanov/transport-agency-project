﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Feonufry.CUI.Actions;
using Feonufry.CUI.Menu.Builders;

namespace TransportAgencyProject
{
    class MadeVehicleType:IAction
    {
        private Town town;

        public MadeVehicleType(Town town)
        {
            this.town = town;
        }

        public void Perform(ActionExecutionContext context)
        {
            context.Out.WriteLine(ConsoleColor.Green, "Город для отправления выбран.");
            var menu = new MenuBuilder()
                .Title("На чем можно добраться до " + town.Name +".")
                .Prompt("Выберите транспорт:")
                .RunnableOnce();
            foreach (var vehicle in Enum.GetNames(typeof (VehicleType)))
            {
                menu.Item(vehicle, new MadeVehicleClass(town, (VehicleType) Enum.Parse(typeof(VehicleType), vehicle)));
            }
                menu
                    .Exit("Отмена")
                    .GetMenu().Run();
            
        }
    }
}
