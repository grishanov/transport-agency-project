﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PointOfDestination:Entity
    {
        
        private string number;
        private DateTime date;
        private int residenceTime;
        private Vehicle vehicle;
        private Settlement settlement;
        private Town departurePlace;
        private TravelyPackage travelyPackage;

        public PointOfDestination(string number, DateTime date, int residenceTime, Vehicle vehicle, Settlement settlement, Town departurePlace, TravelyPackage travelyPackage)
        {
            this.number = number;
            this.date = date;
            this.residenceTime = residenceTime;
            this.vehicle = vehicle;
            this.settlement = settlement;
            this.departurePlace = departurePlace;
            this.travelyPackage = travelyPackage;
        }

        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public int ResidenceTime
        {
            get { return residenceTime; }
            set { residenceTime = value; }
        }

        public Vehicle Vehicle
        {
            get { return vehicle; }
            set { vehicle = value; }
        }

        public Settlement Settlement
        {
            get { return settlement; }
            set { settlement = value; }
        }

        public Town DeparturePlace
        {
            get { return departurePlace; }
            set { departurePlace = value; }
        }

        public TravelyPackage TravelyPackage
        {
            get { return travelyPackage; }
            set { travelyPackage = value; }
        }
    }
}
