﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Settlement:Entity
    {
       
        private Town town;
        private List<Hotel> hotels;
        private List<PrivateCar> privateCars;
        private List<Insurance> insurances;
        
        public Settlement(Town town, List<Hotel> hotels, List<PrivateCar> privateCars, List<Insurance> insurances)
        {
            this.town = town;
            this.hotels = hotels;
            this.privateCars = privateCars;
            this.insurances = insurances;
        }

        public Town Town
        {
            get { return town; }
            set { town = value; }
        }

        public List<Hotel> Hotels
        {
            get { return hotels; }
            set { hotels = value; }
        }

        public List<PrivateCar> PrivateCars
        {
            get { return privateCars; }
            set { privateCars = value; }
        }

        public List<Insurance> Insurances
        {
            get { return insurances; }
            set { Insurances = value; }
        }
    }
}
