﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class TravelyPackage : Entity
    {
        private Hotel hotel;
        private PrivateCar privateCar;
        private Insurance insurance;

        public TravelyPackage(Hotel hotel, PrivateCar privateCar, Insurance insurance)
        {
            this.hotel = hotel;
            this.privateCar = privateCar;
            this.insurance = insurance;
        }

        public Hotel Hotel
        {
            get { return hotel; }
            set { hotel = value; }
        }

        public PrivateCar PrivateCar
        {
            get { return privateCar; }
            set { privateCar = value; }
        }

        public Insurance Insurance
        {
            get { return insurance; }
            set { insurance = value; }
        }
    }
}
