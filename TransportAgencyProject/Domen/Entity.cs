﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    abstract public class Entity
    {
        private readonly Guid id;

        protected Entity()
        {
            id = Guid.NewGuid();
        }

        public Guid Id
        {
            get{ return id; }
        }

    }
}
