﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Hotel:Entity
    {
       
        private string name;
        private HotelClass hotelClass;
        private double dailyCost;

        public Hotel(string name, HotelClass hotelClass, double dailyCost)
        {
            this.name = name;
            this.hotelClass = hotelClass;
            this.dailyCost = dailyCost;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public HotelClass HotelClass
        {
            get { return hotelClass; }
            set { hotelClass = value; }
        }

        public double DailyCost
        {
            get { return dailyCost; }
            set { dailyCost = value; }
        }
    }

    public enum HotelClass
    {
        VeryBad,
        Bad,
        Middle,
        Good,
        VeryGood
    };
}
