﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class VehicleConfigurator
    {
        public List<Vehicle> GetByType(List<Vehicle> vehicles, VehicleType type,Town town)
        {
            var result = new List<Vehicle>();
            foreach(var vehicle in vehicles)
            {
                if (vehicle.Type == type && vehicle.Towns.Contains(town))
                {
                    result.Add(vehicle);
                }
            }
            return result;
        }

        public List<Vehicle> GetByCost(List<Vehicle> vehicles, double beginCost,double finalCost, Town town)
        {
            var result = new List<Vehicle>();
            foreach (var vehicle in vehicles)
            {
                if (vehicle.Cost >= beginCost && vehicle.Cost <= finalCost && vehicle.Towns.Contains(town))
                {
                    result.Add(vehicle);
                }
            }
            return result;
        }

        public List<Vehicle> GetByClass(List<Vehicle> vehicles, VehicleClass vehicleClass, Town town)
        {
            var result = new List<Vehicle>();
            foreach (var vehicle in vehicles)
            {
                if (vehicle.VehicleClass == vehicleClass && vehicle.Towns.Contains(town))
                {
                    result.Add(vehicle);
                }
            }
            return result;
        }
     }
}
