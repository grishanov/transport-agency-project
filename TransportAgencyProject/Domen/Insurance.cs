﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Insurance:Entity
    {
      
        private string number;
        private int percent;
        private double cost;

        public Insurance(int percent, string number, double cost)
        {
            this.percent = percent;
            this.number = number;
            this.cost = cost;
        }

        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        public int Percent
        {
            get { return percent; }
            set { percent = value; }
        }

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }
    }
}
