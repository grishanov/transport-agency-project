﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class TravelyPackageConfigurator
    {
        public List<TravelyPackage> GetByClass(Settlement settlement, TravelyPackageClass travelyPackageClass)
        {
            var result = new List<TravelyPackage>();

            foreach (var hotel in settlement.Hotels)
            {
                foreach (var privateCar in settlement.PrivateCars)
                {
                    foreach (var insurance in settlement.Insurances)
                    {
                        if (travelyPackageClass == TravelyPackageClass.Bad)
                        {
                            if ((hotel.HotelClass == HotelClass.Bad || hotel.HotelClass == HotelClass.VeryBad) && privateCar.Type > CarClass.F
                            && insurance.Percent < 10)
                            {
                                result.Add(new TravelyPackage(hotel, privateCar, insurance));
                            }
                        }
                        if (travelyPackageClass == TravelyPackageClass.Middle)
                        {
                           if ((hotel.HotelClass == HotelClass.Middle || hotel.HotelClass == HotelClass.Good) && privateCar.Type <= CarClass.F 
                           && insurance.Percent >= 10)
                            {
                                result.Add(new TravelyPackage(hotel, privateCar, insurance));
                            }
                        }
                        if (travelyPackageClass == TravelyPackageClass.Good)
                        {
                            if ((hotel.HotelClass == HotelClass.Good || hotel.HotelClass == HotelClass.VeryGood) && privateCar.Type <= CarClass.F
                            && insurance.Percent >= 10)
                            {
                                result.Add(new TravelyPackage(hotel, privateCar, insurance));
                            }
                        }

                    }
                }
            }
            return result;
        }

        public List<TravelyPackage> GetByCost(Settlement settlement, double beginCost, double finalCost, int residenceTime)
        {
            var result = new List<TravelyPackage>();
            foreach (var hotel in settlement.Hotels)
            {
                foreach (var privateCar in settlement.PrivateCars)
                {
                    foreach (var insurance in settlement.Insurances)
                    {
                        if ((hotel.DailyCost * residenceTime + privateCar.Cost + insurance.Cost) >= beginCost && (hotel.DailyCost * residenceTime + privateCar.Cost + insurance.Cost) <= finalCost)
                        {
                            result.Add(new TravelyPackage(hotel, privateCar, insurance));
                        }
                    }
                }
            }
            return result;
        }

        public List<TravelyPackage> GetAll(Settlement settlement)
        {
            var result = new List<TravelyPackage>();
            foreach (var hotel in settlement.Hotels)
            {
                foreach (var privateCar in settlement.PrivateCars)
                {
                    foreach (var insurance in settlement.Insurances)
                    {
                        result.Add(new TravelyPackage(hotel, privateCar, insurance));
                    }
                }
            }

            return result;
        }
    }

    public enum TravelyPackageClass
    {
        Bad,
        Middle,
        Good
    };
}
