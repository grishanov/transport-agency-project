﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Vehicle:Entity
    {
        private string name;       
        private VehicleType type;
        private VehicleClass vehicleClass;
        private double cost;
        private List<Town> towns;

        public Vehicle(string name, VehicleType type, VehicleClass vehicleClass, double cost, List<Town> towns)
        {
            this.name = name;
            this.type = type;
            this.vehicleClass = vehicleClass;
            this.cost = cost;
            this.towns = towns;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public VehicleType Type
        {
            get { return type; }
            set { type = value; }
        }

        public VehicleClass VehicleClass
        {
            get { return vehicleClass; }
            set { vehicleClass = value; }
        }

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public List<Town> Towns
        {
            get { return towns; }
            set { Towns = value; }
        }

        public void AddTown(Town town)
        {
            towns.Add(town);
        }
    }

    public enum VehicleType
    {
        Bus,
        Airplane,
        Train,
        Ship
    };

    public enum VehicleClass
    {
        VeryBad,
        Bad,
        Middle,
        Good,
        VeryGood
    };
}
