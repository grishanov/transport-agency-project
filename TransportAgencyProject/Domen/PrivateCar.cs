﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PrivateCar:Entity
    {
       
        private string model;
        private CarClass type;
        private double cost;

        public PrivateCar(string model, CarClass type, double cost)
        {
            this.model = model;
            this.type = type;
            this.cost = cost;
        }

        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        public CarClass Type
        {
            get { return type; }
            set { type = value; }
        }

        public double Cost
        {
            get { return cost; }
            set { cost = value; }
        }
    }

    public enum CarClass
    {
        A,
        B,
        C,
        D,
        E,
        F,
        S,
        M,
        J
    };
}
